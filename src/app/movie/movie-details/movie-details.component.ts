import { Component, Input, OnInit } from '@angular/core';
import { StorageService } from 'src/app/storage.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  @Input() movie: any;
  imdbLink = 'http://www.imdb.com/title';
  constructor(private storage: StorageService) { }

  ngOnInit() {
    this.storage.getById(this.movie.id)
    .subscribe(
      (response: any) => {
        this.movie = response;
       }
    );
  }

}
