import { Component, Input, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  @Input() movie: any;
  movieDetails = {};
  imgBaseUrl: string | undefined;
  posterUrl: string | undefined;
  display = false;
  displayButton = 'Display details';
  releaseDate: string | undefined;
  constructor(
    private storage: StorageService,
  ) { }

  ngOnInit(): void {
    if (this.movie.poster_path === null) {
      this.posterUrl = 'http://via.placeholder.com/154x218?text=Not+avaliable';
    } else {
      this.imgBaseUrl = this.storage.getImageBaseUrl()
      this.posterUrl = this.imgBaseUrl + 'w154' + this.movie.poster_path;
    }
    if (this.movie.release_date){
      this.releaseDate = this.movie.release_date.substring(0,4);
    }
    else{
      this.releaseDate = '';
    }
  }

  changeButton() {
    this.display = !this.display;
    if (this.display === true) {
      this.displayButton = 'Hide details';
    } else {
      this.displayButton = 'Display details';
    }
  }

}
