import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../movies.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  moviesList = [];
  emptyQuery = false;
  emptyList = false;
  notFound = false;
  p: any;
  constructor(
    private storage: StorageService,
    private movies: MoviesService,
    private route: ActivatedRoute,
  ) {
    movies.searchFilterEmited$.subscribe(
      filter => {
          this.search(filter);
      });
   }

   search(query: string) {
     this.isFilterEmpty(query);
     this.isListEmpty();
     this.storage.search(query)
    .subscribe(
      (response: any) => {
        this.moviesList = response['results'];
        this.isListEmpty();
        this.notFoundMessage(query);
      }
    );
  }

  ngOnInit(): void {
  }

  notFoundMessage(filter: string){
    if (filter !== ''){
      if (this.moviesList.length === 0){
        this.notFound = true;
      }
      else{
        this.notFound = false;
      }
    }
  }

  isFilterEmpty(text: any){
    if (text === ''){
      this.emptyQuery = true;
      this.notFound = false;
      this.moviesList = [];
    }
    else{
      this.emptyQuery = false;
    }
  }

  isListEmpty(){
    if (this.moviesList && this.moviesList.length === 0){
      this.emptyList = false;
    }
    else{
      this.emptyList = true;
    }

  }
}
