import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private apiKey = '?api_key=xxxxxxxxxxxxxxxxxxxxxx';
  private apiUrl = 'https://api.themoviedb.org/';
  imageBaseurl = 'https://image.tmdb.org/t/p/';
  constructor(
    private http: HttpClient,
    ) { }

  getImageBaseUrl() {
    return this.imageBaseurl;
    }

  search(query: string) {
    query = this.convertToSlug(query);
    return this.http.get(this.apiUrl + '3/search/movie' + this.apiKey + '&query=' + query );
  }

  getById(id: any) {
    return this.http.get(this.apiUrl + '3/movie/' + id + this.apiKey);
  }

  convertToSlug(text: string) {
  text = text.toLowerCase()
      .replace(/[^a-z0-9 -]/g, '')
      .replace(/\s+/g, '+')
      .replace(/-+/g, '+');
  return text;
  }
}
