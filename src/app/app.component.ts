import { Component, OnInit } from '@angular/core';
import {} from '@angular/forms';
import { MoviesService } from './movies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  filter = '';
  welcomeMessage = true;
  constructor(
    private movies: MoviesService,
  ) {}

  ngOnInit() {
    this.displayWelcomeMessage();
  }

  search(filter: any) {
    this.movies.emitChange(filter);
    this.displayWelcomeMessage();
  }

  displayWelcomeMessage(){
    if (this.filter === ''){
      this.welcomeMessage = true;
    }
    else{
      this.welcomeMessage = false;
    }
  }
}
